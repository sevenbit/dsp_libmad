/*
 * libmad - MPEG audio decoder library
 * Copyright (C) 2000-2004 Underbit Technologies, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * $Id: minimad.c,v 1.4 2004/01/23 09:41:32 rob Exp $
 */

# include <stdlib.h>
# include "mad.h"

# ifdef HAVE_CONFIG_H
#  include "config.h"
# endif
# include "global.h"
# include "fixed.h"

# include "predefined.h"

# include "test1_as_int.h"
# include "test1_out_as_int.h"

unsigned char file_buf[MAX_FILE_SIZE];
unsigned char sync_buf[DECODER_SYNC_SIZE];
FILE *out_file;

/*
 * This is perhaps the simplest example use of the MAD high-level API.
 * Standard input is mapped into memory via mmap(), then the high-level API
 * is invoked with three callbacks: input, output, and error. The output
 * callback converts MAD's high-resolution PCM samples to 16 bits, then
 * writes them to standard output in little-endian, stereo-interleaved
 * format.
 */

static int decode(unsigned char const *, unsigned long);
/*
 * This is a private message structure. A generic pointer to this structure
 * is passed to each of the callback functions. Put here any data you need
 * to access from within the callbacks.
 */

struct tbuffer {
  unsigned char const *start;
  unsigned long length;
};


void read_file_to_buffer(char* name,struct tbuffer* buffer)
{
	FILE *file;
	long int len;
	
	file = fopen(name, "rb");
	if(!file)
	{
		fprintf(stderr, "unable to open file %s\n", name);
		return;
	}
	

	fseek(file, 0, SEEK_END);
	len = ftell(file); 
	fseek(file, 0, SEEK_SET);

	fprintf(stderr, "len: %d\n", len);
	
	buffer->length = len;
#ifdef WITHOUT_MALLOC
	buffer->start = file_buf;
#else
	buffer->start = malloc(buffer->length+1);
#endif
	
	
	if(!buffer->start)
	{
		fprintf(stderr, "memory error\n");
		fclose(file);
		return;
	}

	fread((void *)(buffer->start), buffer->length, 1, file);
	fclose(file);

}



/* Place program arguments in the following string, or delete the definition
   if arguments will be passed from the command line. */
const char __argv_string[] = "test1.mp3";

/*
 * argv[1] - input mp3-file to decode
 * argv[2] - output file name
 */
int main(int argc, char *argv[])
{
	


#define WITHOUT_MMAP
	struct tbuffer *buffer;

#ifdef OUT_FILE_NAME


  out_file = fopen(OUT_FILE_NAME, "wb");
  if(!out_file)
  {
  	fprintf(stderr, "could'nt open file to write\n");  	
  	exit(1);
  }
#endif
	
/*don't forget to free used mem*/
#ifdef WITHOUT_MMAP
	
	if(argc == 1)
	{
		fprintf(stderr,	"invalid agruments\n");
//		fprintf(stderr,	"invalid agruments, must be _ [input_file] [output_file]\n");
		exit(1);
	}

	buffer = malloc(sizeof(buffer));
	read_file_to_buffer(argv[1], buffer);
	if(!buffer->start)
	{
		fprintf(stderr,"read file error");
		exit(2);
	}
	
	decode(buffer->start, buffer->length);
#ifdef OUT_FILE_NAME
	fclose(out_file);
#endif

	return 0;
	
#else
	/*
  struct stat stat;
  void *fdm;


  if (argc != 1)
    return 1;

  if (fstat(STDIN_FILENO, &stat) == -1 ||
      stat.st_size == 0)
    return 2;

  fdm = mmap(0, stat.st_size, PROT_READ, MAP_SHARED, STDIN_FILENO, 0);
  if (fdm == MAP_FAILED)
    return 3;

  decode(fdm, stat.st_size);

  if (munmap(fdm, stat.st_size) == -1)
    return 4;

  return 0;
  */
#endif	
}

/*
 * This is the input callback. The purpose of this callback is to (re)fill
 * the stream buffer which is to be decoded. In this example, an entire file
 * has been mapped into memory, so we just call mad_stream_buffer() with the
 * address and length of the mapping. When this callback is called a second
 * time, we are finished decoding.
 */

static
enum mad_flow input(void *data,
		    struct mad_stream *stream)
{
  struct tbuffer *buffer = data;

  if (!buffer->length)
    return MAD_FLOW_STOP;

  mad_stream_buffer(stream, buffer->start, buffer->length);

  buffer->length = 0;

  return MAD_FLOW_CONTINUE;
}

/*
 * The following utility routine performs simple rounding, clipping, and
 * scaling of MAD's high-resolution samples down to 16 bits. It does not
 * perform any dithering or noise shaping, which would be recommended to
 * obtain any exceptional audio quality. It is therefore not recommended to
 * use this routine if high-quality output is desired.
 */
#define inline

static inline
signed int scale(mad_fixed_t sample)
{
  /* round */
  sample += (1L << (MAD_F_FRACBITS - 16));

  /* clip */
  if (sample >= MAD_F_ONE)
    sample = MAD_F_ONE - 1;
  else if (sample < -MAD_F_ONE)
    sample = -MAD_F_ONE;

  /* quantize */
  return sample >> (MAD_F_FRACBITS + 1 - 16);
}

/*
 * This is the output callback function. It is called after each frame of
 * MPEG audio data has been completely decoded. The purpose of this callback
 * is to output (or play) the decoded PCM audio.
 */
#define GEN_SINGLE_PCM

static enum mad_flow output(void *data,struct mad_header const *header,struct mad_pcm *pcm)
{
  unsigned int nchannels, nsamples;
  mad_fixed_t const *left_ch, *right_ch;
#ifdef OUT_FILE_NAME
  static int i = 0;
	char filename[10];
	FILE *sample_file;
#endif
  /* pcm->samplerate contains the sampling frequency */

  nchannels = pcm->channels;
  nsamples  = pcm->length;
  left_ch   = pcm->samples[0];
  right_ch  = pcm->samples[1];


#ifdef OUT_FILE_NAME


#ifdef GEN_SINGLE_PCM
  if(i==0)
    sample_file =  fopen("decode_out.wav", "wb");
  else
    sample_file =  fopen("decode_out.wav", "ab");
    
  fprintf(stderr,  "out %d\n", i);
  i++;
#else
  fprintf(stderr,  "out %d\n", ++i);
	sprintf(filename,"out_%d", i);

  sample_file =  fopen(filename, "wb");
#endif	
  if(!sample_file)
	{
		fprintf(stderr, "could'nt open sample file\n");
	}

  
  while (nsamples--) 
  {
    signed int sample;
		signed int s1, s2, s3, s4;

    /* output sample(s) in 16-bit signed little-endian PCM */

    sample = scale(*left_ch++);

		s1 = (sample >> 0) & 0xff;
		s2 = (sample >> 8) & 0xff;

    fputc(s1, out_file);
    fputc(s2, out_file);

		fputc(s1, sample_file);
		fputc(s2, sample_file);

    if (nchannels == 2) {
      sample = scale(*right_ch++);

			s3 = (sample >> 0) & 0xff;
			s4 = (sample >> 8) & 0xff;
			
      fputc(s3, out_file);
      fputc(s4, out_file);

			fputc(s3, sample_file);
			fputc(s4, sample_file);
    }
  }  
	fclose(sample_file);


#else

  while (nsamples--) {
    signed int sample;

    /* output sample(s) in 16-bit signed little-endian PCM */

    sample = scale(*left_ch++);
    putchar((sample >> 0) & 0xff);
    putchar((sample >> 8) & 0xff);

    if (nchannels == 2) {
      sample = scale(*right_ch++);
      putchar((sample >> 0) & 0xff);
      putchar((sample >> 8) & 0xff);
    }
  }
#endif

  return MAD_FLOW_CONTINUE;
}

/*
 * This is the error callback function. It is called whenever a decoding
 * error occurs. The error is indicated by stream->error; the list of
 * possible MAD_ERROR_* errors can be found in the mad.h (or stream.h)
 * header file.
 */

static
enum mad_flow error(void *data,
		    struct mad_stream *stream,
		    struct mad_frame *frame)
{
  struct tbuffer *buffer = data;

  fprintf(stderr, "decoding error 0x%04x (%s) at byte offset %u\n",
	  stream->error, mad_stream_errorstr(stream),
	  stream->this_frame - buffer->start);

  /* return MAD_FLOW_BREAK here to stop decoding (and propagate an error) */

  return MAD_FLOW_CONTINUE;
}

/*
 * This is the function called by main() above to perform all the decoding.
 * It instantiates a decoder object and configures it with the input,
 * output, and error callback functions above. A single call to
 * mad_decoder_run() continues until a callback function returns
 * MAD_FLOW_STOP (to stop decoding) or MAD_FLOW_BREAK (to stop decoding and
 * signal an error).
 */

static
int decode(unsigned char const *start, unsigned long length)
{
  struct tbuffer buffer;
  struct mad_decoder decoder;
  int result;

  /* initialize our private message structure */

  buffer.start  = start;
  buffer.length = length;

  /* configure input, output, and error functions */

  mad_decoder_init(&decoder, &buffer,
		   input, 0 /* header */, 0 /* filter */, output,
		   error, 0 /* message */);

  /* start decoding */

  result = mad_decoder_run(&decoder, MAD_DECODER_MODE_SYNC);

  /* release the decoder */

  mad_decoder_finish(&decoder);

  return result;
}
