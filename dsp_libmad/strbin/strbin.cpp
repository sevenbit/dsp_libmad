#include <iostream>
#include <fstream>

using namespace std;
#define DEFAULT_INPUT_FILE		"test1.mp3"
#define DEFAULT_OUTPUT_FILE		"test1_str_bin"


// A macro to disallow the copy constructor and operator= functions
// This should be used in the private: declarations for a class
#define DISALLOW_COPY_AND_ASSIGN(TypeName) \
  TypeName(const TypeName&);               \
  void operator=(const TypeName&)

class file_guard {
	DISALLOW_COPY_AND_ASSIGN(file_guard);
	mutable ifstream is;	

	int length() const{
		int len = 0;
		is.seekg(0, ios::end);	
		len = is.tellg();
		is.seekg(0, ios::beg);
	
		return len;
	}

public:

	file_guard(string name, ios_base::openmode mode) {
		is.open(name.c_str(), mode);
	}

	~file_guard() {
		is.close();
	}

	friend ostream& operator<< (ostream &out, const file_guard &guard);
};

void print_buf_as_str(ostream &out, const char* buffer, int buf_size) {
	for(int i = 0; i < buf_size; ++i)
		out << (int)buffer[i] << ",";
}

ostream& operator<< (ostream &out, const file_guard &guard) {
	const int buf_size =		256 * 256;
	const int len =			  	guard.length();
	const int block_count =	len / buf_size;

	char buffer[buf_size];

	for(int i = 0; i < block_count; ++i) {
		guard.is.read(buffer, buf_size);		
		print_buf_as_str(out, buffer, buf_size);
	}

	const int block_rem =	len % buf_size;
	if(block_rem){
		guard.is.read(buffer, block_rem);
		print_buf_as_str(out, buffer, block_rem);
	}

	return out;
}


void read_bin_file(string fin, string fout) {
	file_guard	in(fin, ios::in | ios::binary);
	ofstream		out(fout.c_str());
	
	out << in;
	out.close();
}

int main(int argc, char* argv[]) {
	string input_file, output_file;
	if(argc == 1) {
		input_file	= DEFAULT_INPUT_FILE;
		output_file = DEFAULT_OUTPUT_FILE;
	} else if(argc == 3) {
		input_file	= argv[1];
		output_file = argv[2];
	} else {
		cerr << "invalid arguments!" << endl;
		exit(1);
	}

	read_bin_file(input_file, output_file);

	return 0;
}
